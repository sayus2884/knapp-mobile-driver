import React, { Component } from 'react';
import { Font } from "expo";
import Meteor from 'react-native-meteor';
import { Text, View, StatusBar } from 'react-native';
import { Container, Header, Root } from 'native-base';
import { StackNavigator } from 'react-navigation';
import getTheme from './native-base-theme/components';

// Main Application
import App from './ui/App'

// Connect to web server through socket
Meteor.connect('ws://www.knappdelivery.com/websocket');


export default class Knapp extends Component {

   render() {
      return (
         <Root>
            <App />
         </Root>
      )
   }
}
