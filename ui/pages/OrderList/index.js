import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Container, Content, Header, Left, Icon, Button, List, ListItem } from 'native-base';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import MapView from 'react-native-maps';

class OrderList extends Component {
   render() {
      const { goBack } = this.props.navigation;

      return (
         <Container>

            <Header>
               <Left>
                  <Button transparent onPress={() => goBack()}>
                     <Icon name='arrow-back' />
                  </Button>
               </Left>
            </Header>

            <Content>
               <List>
                  {this.renderOrders()}
               </List>
            </Content>


         </Container>
      );
   }

   renderOrders(){
      const orders = this.props.orders;
		const isLoading = this.props.loading;

      const { navigate } = this.props.navigation;

      return isLoading ? (
			<Text>Loading Orders</Text>
		) : orders.map( (order) => (
         <ListItem key={order._id} button onPress={() => navigate('OrderDetails', { order })}>
            <Text>{order.recipient}</Text>
         </ListItem>
      ))
   }
}

export default createContainer(() => {

   const subscription = Meteor.subscribe('deliveries', {});
   const loading = !subscription.ready();
   const orders = Meteor.collection('orders').find();

   return { orders, loading }

}, OrderList);
