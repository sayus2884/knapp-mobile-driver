import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Container, Content, Header, Button } from 'native-base';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import MapView from 'react-native-maps';

class Login extends Component {
   render() {
      const { navigate } = this.props.navigation;

      return (
         <Container>

            <Header>
            </Header>

            <Content>

               <Text>This is the login</Text>
               <Button title="Go to EstablishmentList" onPress={() => navigate('EstablishmentList')}></Button>

            </Content>


         </Container>
      );
   }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  scrollview: {
    alignItems: 'center',
    paddingVertical: 40,
  },
  map: {
     width: '100%',
     height: '100%',
  },
});

export default Login
