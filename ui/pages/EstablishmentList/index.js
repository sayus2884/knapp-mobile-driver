import React, { Component } from 'react'
import Meteor, { createContainer } from 'react-native-meteor';
import { Container, Header, Left, Icon, Button, Body, Title, Right  } from 'native-base';
import { StyleSheet, Text, View } from 'react-native';

class EstablishmentList extends Component {
   render()
   {

      return (
         <Container style={{backgroundColor: "#FFF"}}>

            <Header>
               <Left>
                  <Button transparent onPress={() => this.props.navigation.navigate('Login')}>
                     <Icon name="menu" />
                  </Button>
               </Left>

               <Body>
                  <Title>List</Title>
               </Body>

               <Right />
            </Header>

            {this.renderEstablishments()}

            <Text>This is the list of Establishments</Text>
         </Container>
      )
   }

   renderEstablishments(){
      const establishments = this.props.establishments;
		const isLoading = this.props.loading;

      return isLoading ? (
			<Text>Loading Establishments</Text>
		) : establishments.map( (establishment) => (

         <Text key={establishment._id}>{establishment.name}</Text>

      ))
   }
}

export default createContainer(() => {

   const subscription = Meteor.subscribe('establishments', {});
   const loading = !subscription.ready();
   const establishments = Meteor.collection('establishments').find();

   return { establishments, loading }

}, EstablishmentList);
