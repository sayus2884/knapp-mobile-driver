import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Right, Left, Footer, FooterTab, Button, Text, Header } from 'native-base';
import {StyleSheet} from 'react-native';

class FooterNotification extends Component {
   constructor(){
      super();

      this.state = {
         delivering: false,
         currentOrder: ''
      };
   }

   render() {
      return (
         <Header>
            <Left>
               <Text>Mamap's to IIT</Text>
            </Left>
            <Right>
               <Button info small style={styles.button}>
                  <Text>Next</Text>
               </Button>
               <Button info small style={styles.button}>
                  <Text>Deliver</Text>
               </Button>
            </Right>
         </Header>
      );
   }
}

const styles = StyleSheet.create({
  button: {
    marginLeft: 5,
  },
});

export default FooterNotification
