import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Container, Header, Right, Icon, Footer, FooterTab, Button, Content } from 'native-base';
import { StyleSheet, Text, View, Platform, PermissionsAndroid } from 'react-native';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';

import FooterNotification from './FooterNotification';

class MapNavigation extends Component {
   constructor(props) {
      super(props);
      this.mounted = false;
      this.state = {
         myPosition: null,
         coords: []
      };
   }

   render() {
      const { navigate } = this.props.navigation;

      return (
         <Container>

            <Header>
               <Right>
                  <Button transparent onPress={() => navigate('OrderList')}>
                     <Icon name='menu' />
                  </Button>
               </Right>
            </Header>

            <Content>
               <Container>
                  <MapView
                     style={styles.map}
                     initialRegion={{
                        latitude: 8.476082,
                        longitude: 124.647939,
                        latitudeDelta: 0.0046,
                        longitudeDelta: 0.0021,
                     }}
                     >
                  <MapView.Marker draggable
                     coordinate={{
                        latitude: 8.476082,
                        longitude: 124.647939,
                     }}
                     />

                  <MapView.Polyline
                     coordinates={this.state.coords}
                     strokeWidth={4}
                     strokeColor="red"/>

                  </MapView>
               </Container>
            </Content>

            <FooterNotification />

         </Container>
      );
   }

   // fetch directions and decode polylines
   async getDirections(startLoc, destinationLoc) {
      try {
         let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`)
         let respJson = await resp.json();
         let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
         let coords = points.map((point, index) => {
            return  {
               latitude : point[0],
               longitude : point[1]
            }
         })
         this.setState({coords: coords})
         return coords
      } catch(error) {
         return error
      }
   }

   componentDidMount() {
      this.mounted = true;
      // If you supply a coordinate prop, we won't try to track location automatically
      if (this.props.coordinate) return;

      if (Platform.OS === 'android') {
         PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
         // .then(granted => {
         //    if (granted && this.mounted) this.watchLocation();
         // });
      } else {
         // this.watchLocation();
      }

      this.getDirections("8.476082, 124.647939", "8.474004366851322, 124.64811086654663")
   }

   // watchLocation() {
   //    // eslint-disable-next-line no-undef
   //    this.watchID = navigator.geolocation.watchPosition((position) => {
   //       const myLastPosition = this.state.myPosition;
   //       const myPosition = position.coords;
   //       if (!isEqual(myPosition, myLastPosition)) {
   //          this.setState({ myPosition });
   //       }
   //    }, null, this.props.geolocationOptions);
   // }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  scrollview: {
    alignItems: 'center',
    paddingVertical: 40,
  },
  map: {
     width: '100%',
     height: '100%',
  },
});

export default createContainer(() => {

   const subscription = Meteor.subscribe('deliveries', {});
   const loading = !subscription.ready();
   const orders = Meteor.collection('orders').find({
      delivering: false
   });

   return { orders, loading }

}, MapNavigation)
