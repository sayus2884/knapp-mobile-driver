import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Meteor, { createContainer } from 'react-native-meteor';
import { Container, Content, Header, Left, Right, Icon, Button, List, ListItem, Body, H2, Text } from 'native-base';
import { StyleSheet } from 'react-native';
import MapView from 'react-native-maps';

import ListItemData from '../../components/ListItemData';

class OrderDetails extends Component {
   render() {
      const { navigate, goBack, state } = this.props.navigation;
      const order = state.params.order;

      return (
         <Container>

            <Header>
               <Left>
                  <Button transparent onPress={() => goBack()}>
                     <Icon name='arrow-back' />
                  </Button>
               </Left>

               <Right>
                  <Button info small onPress={() => goBack()}>
                     <Text>Deliver</Text>
                  </Button>
               </Right>
            </Header>

            <Content>
               <List>
                  <ListItemData value={order.recipient} label="Recipient"/>
                  <ListItemData value={order.contact} label="Contact"/>
                  <ListItemData value={order.address} label="Address"/>
               </List>

               <Container>
                  <MapView
                     style={styles.map}
                     initialRegion={{
                        latitude: 8.476082,
                        longitude: 124.647939,
                        latitudeDelta: 0.0046,
                        longitudeDelta: 0.0021,
                     }}
                     >
                  </MapView>
               </Container>
            </Content>

         </Container>
      );
   }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  scrollview: {
    alignItems: 'center',
    paddingVertical: 40,
  },
  map: {
     width: '100%',
     height: '100%',
  },
});

export default OrderDetails;
