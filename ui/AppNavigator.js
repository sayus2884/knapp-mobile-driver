import React, { Component } from 'react';
import { Font } from "expo";
import Meteor from 'react-native-meteor';
import { Text, View, StatusBar } from 'react-native';
import { Container, Header, Root } from 'native-base';
import { StackNavigator } from 'react-navigation';

import Login from './pages/Login';
import MapNavigation from './pages/MapNavigation';
import OrderList from './pages/OrderList';
import OrderDetails from './pages/OrderDetails';

// Create Navigator
export default AppNavigator = StackNavigator({
   Login: { screen: Login },
   MapNavigation: { screen: MapNavigation },
   OrderList: { screen: OrderList },
   OrderDetails: { screen: OrderDetails },

}, {
   initialRouteName: 'MapNavigation',
   headerMode: 'none'
});
