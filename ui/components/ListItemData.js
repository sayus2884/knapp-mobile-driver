import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Meteor, { createContainer } from 'react-native-meteor';
import { ListItem, Body, H2, Text } from 'native-base';

class ListItemData extends Component {
   render() {
      const { value, label } = this.props;

      return (
         <ListItem>
            <Body>
               <Text>{value}</Text>
               <Text note>{label}</Text>
            </Body>
         </ListItem>
      );
   }
}

ListItemData.propTypes = {
   value: PropTypes.string.isRequired,
   label: PropTypes.string
}

export default ListItemData;
